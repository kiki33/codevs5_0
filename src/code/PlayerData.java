package code;

import java.util.Arrays;
import java.util.Scanner;

public class PlayerData {
	//スキルポイント
	public int point;
	//マップ情報
	public char[][] map;
	//忍者
	public Ninja[] ninjas;
	//優先度マップ
	public float[][] primaryMap;
	//犬リスト
	public Dog[] dogList;
	//犬マップ
	public short[][] dogMap;
	//アイテムマップ
	public boolean[][] itemMap;
	//スキル使用回数
	public int[] useSkill;
	//直前にそのスキルを使ったかどうか
	public boolean[] justUseSkill;
	//次の行動でソウルをとれる可能性があるソウルの数
	public int nextItem;
	public PlayerData() {

	}

	public void input(Scanner sc) {
		point = sc.nextInt();
		Main.map_row = sc.nextInt();
		Main.map_col = sc.nextInt();
		primaryMap = new float[Main.map_row][Main.map_col];
		int n;
		//wallMap = new boolean[map_row][map_col];
		map = new char[Main.map_row][Main.map_col];
		for (int r = 0; r < Main.map_row; ++r) {
			String line = sc.next();
			for (int c = 0; c < Main.map_col; ++c) {
				map[r][c] = line.charAt(c);
				//wallMap[r][c] = line.charAt(c) == '_';
			}
		}

		// character
		n = sc.nextInt();
		ninjas = new Ninja[n];
		for (int i = 0; i < n; ++i) {
			int id = sc.nextInt(), row = sc.nextInt(), col = sc.nextInt();
			ninjas[i] = new Ninja(id,row, col);
		}
		// zombie
		dogMap = new short[Main.map_row][Main.map_col];
		for (short[] i:dogMap) {
			Arrays.fill(i, (short)-1);
		}
		n = sc.nextInt();
		dogList = new Dog[n];
		for (int i = 0; i < n; ++i) {
			short id = (short)sc.nextInt();
			int row = sc.nextInt(), col = sc.nextInt();
			dogMap[row][col] = id;
			dogList[i] = new Dog(id, row, col);
		}
		// item
		itemMap = new boolean[Main.map_row][Main.map_col];
		n = sc.nextInt();
		for (int i = 0; i < n; ++i) {
			int row = sc.nextInt(), col = sc.nextInt();
			itemMap[row][col] = true;
		}

		int[] beforUseSkill = useSkill;
		justUseSkill = new boolean[Main.skills];
		useSkill = new int[Main.skills];
		for (int i = 0; i < Main.skills; ++i) {
			useSkill[i] = sc.nextInt();
		}
		if (beforUseSkill != null) {
			for (int i=0; i < Main.skills; i++) {
				justUseSkill[i] = useSkill[i] > beforUseSkill[i];
			}
		}
		//データ処理
	}
	//データ処理
	public void calcData() {
		//次取れそうなアイテム数の計算
		nextItem = 0;
		int dist = Main.skillCost[Main.SPEED_SKILL]<= point?3:2;
		for(int r=0; r < Main.map_row; r++) {
			for (int c=0; c < Main.map_col; c++) {
				if (itemMap[r][c]) {
					for(Ninja n:ninjas) {
						if (Main.getDist(r, c, n.row, n.col) <= dist) {
							nextItem++;
							break;
						}
					}
				}
			}
		}
		nextItem = Math.min(nextItem, dist*2);//最大移動距離＊忍者の数までしかとれない
	}

	//岩の移動を考慮してその方向に移動できるかどうか
	public boolean canMove(int r, int c, int d,char[][] map,short[][] dogMap, Ninja[] ninjas) {
		return MapCalc.canMove(r, c,d, map,dogMap,ninjas);
	}
	//その方向から移動可能な石かどうか
	public boolean isMoveStone(int r, int c, int d) {
		return MapCalc.isMoveStone(r, c, d, map, dogMap, ninjas);
	}
	//岩かどうか
	public boolean isStone(int r, int c) {
		return MapCalc.isRock(r, c, map);
	}

}
