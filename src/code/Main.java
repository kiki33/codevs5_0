package code;
import java.util.Scanner;

import code.strategy.NormalStrategy;
import code.strategy.Strategy;
import code.strategy.skill.RowCostSwordSkill;

public class Main {
	public static final char FLOOR = '_';
	public static final char WALL = 'W';
	public static final char ROCK = 'O';
	//スキルの番号
	public static final int SPEED_SKILL = 0;
	public static final int MY_ROCK_SKILL = 1;
	public static final int OPP_ROCK_SKILL = 2;
	public static final int MY_THUNDER_SKILL = 3;
	public static final int OPP_THUNDER_SKILL = 4;
	public static final int MY_AVATAR_SKILL = 5;
	public static final int OPP_AVATAR_SKILL = 6;
	public static final int SWORD_SKILL = 7;
	public static int map_row, map_col;


	public static final void main(String args[]) {
		new Main().solve();
	}

	void solve() {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("SPADE3");
			System.out.flush();
			while (true) {
				System.out.print(think(sc));
				System.out.flush();
			}
		}
	}

	public static final int dx[] = { -1, 0, 0, 1 , 0 };
	public static final int dy[] = {  0,-1, 1, 0 , 0 };
	public static final String ds[] = { "U","L", "R", "D" ,""};
	PlayerData mine = new PlayerData(), opp = new PlayerData();
	public static int skills;
	public static int skillCost[];
	public static boolean isRowSword;
	//戦略
	public Strategy strategy;
	//出力用
	StringBuilder res;
	public Main() {
		/*int minute = Calendar.getInstance().get(Calendar.MINUTE);
		System.err.println(minute);
		if (minute < 30) {*/
			strategy = new NormalStrategy(mine, opp);
		/*}else {
			strategy = new NoStrategy(mine, opp);
		}*/
	}

	String think(Scanner sc) {
		res = new StringBuilder();
		long millitime = sc.nextLong();
		skills = sc.nextInt();
		skillCost = new int[skills];
		for (int i = 0; i < skillCost.length; ++i) {
			skillCost[i] = sc.nextInt();
		}
		isRowSword = skillCost[SWORD_SKILL] <= RowCostSwordSkill.MAX_COST;
		//自身のでデータ読み込み
		mine.input(sc);


		//enemydata
		//敵データの読み込み
		opp.input(sc);


		//出力

		res.append(strategy.calcOrder());
		/*skillPre();
		boolean isSkill = useSkill();

		preSet();
		for (int i = 0; i < mine.ninjas.length; ++i) {
			res.append(order(mine.ninjas[i])).append("\n");
		}

		res.insert(0,(mine.ninjas.length + (isSkill?1:0)) + "\n");*/
		return res.toString();
	}


	//範囲内かの判定
	public static boolean isIn(int row, int col) {
		return !(row < 0 || col < 0 || map_row <= row || map_col <= col);
	}
	//指定の座標から指定の座標への有効な道を算出(壁を考慮してなかった)
	String getRoot(int r, int c, int nr, int nc) {
		String ret = "";
		int dist = getDist(r,c, nr, nc) ;
		if (dist == 0) {
			return "";
		}else if (dist == 1){
			if (r < nr) {
				return "R";
			}
			if (r > nr) {
				return "L";
			}
			if (c < nc) {
				return "D";
			}
			if (c > nc) {
				return "U";
			}
		}else  {
			boolean f = true;//ルート決めたかのフラグ
			int mr = Math.min(r, nr);
			int pr = Math.max(r, nr);
			int mc = Math.min(c, nc);
			int pc = Math.max(c, nc);
			LOOP:for (int x=mr; x <= pr; x++) {
				for (int y=mc; y <= pc; y++) {
					if (x == r && y == c) continue;
					if (x == nr && y == nc) continue;
					if (mine.itemMap[x][y]) {
						ret = getRoot(r, c, x, y) + getRoot(x, y, nr, nc);
						f = false;
						break LOOP;
					}
				}
			}
			if (f) {//決めてなかったら
				if (r < nr) {
					ret = "R" + getRoot(r+1,c, nr, nc);
				}else if (r > nr) {
					ret = "L" + getRoot(r-1,c, nr, nc);
				}else if (c < nc) {
					ret = "D" + getRoot(r,c+1, nr, nc);
				}else if (c > nc) {
					ret = "U" + getRoot(r,c-1, nr, nc);
				}
			}
		}
		return ret;
	}
	//距離の計算
	public static int getDist(int r, int c, int nr, int nc) {
		return Math.abs(r - nr) + Math.abs(c - nc);
	}

	//デバッグ出力
	public static void printMap(double[][] map) {
		System.err.println("map");
		for (int i=0; i < map.length; i++) {
			for (int t=0; t < map[i].length; t++)  {
				System.err.print(map[i][t]+",");
			}
			System.err.println();
		}
	}
	//デバッグ出力
	public static void printMap(int[][] map) {
		System.err.println("map");
		for (int i=0; i < map.length; i++) {
			for (int t=0; t < map[i].length; t++)  {
				System.err.print(map[i][t]+",");
			}
			System.err.println();
		}
	}

}
