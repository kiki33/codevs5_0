package code;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Dog implements Comparable<Dog> {
	//static boolean debug = true;
	static final int MAX_INT = 1000;
	public short id;
	public int row, col;
	public int dist;

	public Dog(short id,int row, int col) {
		this.id = id;
		this.row = row;
		this.col = col;
	}

	public boolean equalPos(int r, int c) {
		return r == row && c == col;
	}
	@Override
	public int compareTo(Dog o) {
		return (this.dist * 1000 + this.id) - (o.dist * 1000 + o.id);

	}

	public static short[][] calcDog(PlayerData data) {
		return calcDog(data.ninjas[0].row, data.ninjas[0].col,
				data.ninjas[1].row, data.ninjas[1].col, data.dogMap, data.map);
	}
	public static short[][] calcDog(PlayerData data
			,short[][] dogMap, char[][] map) {
		return calcDog(data.ninjas[0].row, data.ninjas[0].col,
				data.ninjas[1].row, data.ninjas[1].col, dogMap, map);
	}

	//忍者の座標と地形から犬の次の場所を計算して返す。
	//1つしか無いときは-1を座標にする
	public static short[][] calcDog(int r1, int c1, int r2, int c2
			,short[][] dogMap, char[][] map) {
		//System.err.println("pre");
		short[][] copyDogMap = MapCalc.copyMap(dogMap);
		int[][] distMap = new int[Main.map_row][Main.map_col];
		ArrayList<Dog> dogList = new ArrayList<Dog>();
		for (int[] i: distMap) {
			Arrays.fill(i, MAX_INT);
		}
		ArrayList<Point> list = new ArrayList<Point>();
		list.add(new Point(r1,c1));
		distMap[r1][c1] = 0;
		if (r2 != -1) {
			list.add(new Point(r2,c2));
			distMap[r2][c2] = 0;
		}
		int index = 1;
		//System.err.println("dist");
		while (!list.isEmpty()) {
			//if (debug) Main.printMap(distMap);
			ArrayList<Point> nlist = new ArrayList<Point>();

			for (Point p: list) {
				//distMap[p.x][p.y] = index;
				//まず犬を登録
				if (dogMap[p.x][p.y] != -1) {
					Dog dog = new Dog(dogMap[p.x][p.y], p.x, p.y);
					dog.dist = index;
					dogList.add(dog);
				}
				for (int i=0; i < 4; i++) {
					int nr = p.x + Main.dx[i], nc = p.y + Main.dy[i];
					if (Main.isIn(nr, nc) && map[nr][nc]== Main.FLOOR
							&&distMap[nr][nc] == MAX_INT) {
						nlist.add(new Point(nr, nc));
						distMap[nr][nc] = index;
					}
				}
			}
			//System.err.println();
			//後処理
			list = nlist;
			index++;
		}
		//debug = false;
		//Main.printMap(distMap);
		//犬を動かす
		//System.err.println("dog");
		//id昇順ソート
		Collections.sort(dogList);


		for (Dog d:dogList) {
			//System.err.print(d.id+",");
			//4方向確かめる
			for (int i=0; i < 4; i++) {
				int nr = d.row + Main.dx[i], nc = d.col + Main.dy[i];
				if (map[nr][nc] == Main.FLOOR && copyDogMap[nr][nc] == -1) {//移動可能条件
					//距離短縮条件
					if (distMap[nr][nc] < distMap[d.row][d.col]) {
						//その方向に移動
						copyDogMap[nr][nc] = copyDogMap[d.row][d.col];
						copyDogMap[d.row][d.col] = -1;
						break;
					}
				}
			}
		}
		//System.err.println("end");
		return copyDogMap;
	}
	//犬を追加したマップを作製する
	public static short[][] addDog(int r1, int c1, int r2, int c2
			,short[][] dogMap, char[][] map,int addNum) {
		short[][] copyDogMap = MapCalc.copyMap(dogMap);
		//距離マップ
		int[][] distMap = MapCalc.getDistMap(r1, c1, r2, c2, map);

		for (int i=0; i < addNum; i++) {
			//最長の場所を探す
			int max = -1;
			short maxId = -1;
			int nr = -1, nc = -1;
			for (int r = 0; r < Main.map_row; r++) {
				for (int c = 0; c < Main.map_col; c++) {
					maxId = (short) Math.max(maxId, copyDogMap[r][c]);
					//床で犬がいなければ
					if (map[r][c] == Main.FLOOR && copyDogMap[r][c] == -1) {
						int dist = distMap[r][c];
						//到達不可の場所じゃなくて
						if (dist != MapCalc.MAX_INT){
							if (dist > max) {//大きいなら
								max = dist;
								nr = r;
								nc = c;
							}
						}
					}
				}
			}
			if (max == -1) {//出す場所ないならやめる
				break;
			}

			//犬を追加
			copyDogMap[nr][nc] = (short) (maxId +1);
		}



		return copyDogMap;
	}

	//犬が近くにいるかを返す。周囲４ますとそのマス
	public static boolean isNearDog(int r, int c, short[][] dogMap) {
		for (int i=0; i < 5; i++) {
			int nr = r + Main.dx[i], nc = c + Main.dy[i];
			//犬なら
			if (Main.isIn(nr, nc) && dogMap[nr][nc] != -1){
				return true;
			}
		}
		return false;
	}


}
