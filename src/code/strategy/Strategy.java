package code.strategy;

import code.PlayerData;
import code.strategy.move.Move;

public abstract class Strategy {

	public PlayerData mine;
	public PlayerData opp;
	
	public Move move;
	
	public Strategy(PlayerData mine, PlayerData opp) {
		this.mine = mine;
		this.opp = opp;
	}
	
	public abstract String calcOrder();

}
