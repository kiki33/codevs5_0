package code.strategy;

import java.util.Random;

import code.Main;
import code.PlayerData;
import code.strategy.move.ParallelBeamSearchMove;
import code.strategy.skill.ClincherOppAvatarSkill;
import code.strategy.skill.ClincherOppRockSkill;
import code.strategy.skill.DogRootOppThunderSkill;
import code.strategy.skill.MakeBadOppAVaterSkill;
import code.strategy.skill.RootStopOppAvatarSkill;
import code.strategy.skill.RootStopOppRockSkill;
import code.strategy.skill.RowCostSwordSkill;
import code.strategy.skill.SkillDecision;
import code.strategy.skill.SoulStopOppRockSkill;

public class NormalStrategy extends Strategy {
	//岩を落とす時の犬との距離
	public static final int ROCK_SKILL_DIST = 1;
	public static final int NO_ATTACK_SWORD_COST = 8;

	//乱数生成器
	Random rand = new Random();

	SkillDecision soulRock,oppRock,clincherRock, badAvatar, oppAvatar,sword, oppThunder,clincherAvatar;
	//
	//出力用ビルダー
	StringBuilder res;
	public NormalStrategy(PlayerData mine, PlayerData opp) {
		super(mine, opp);
		this.move = new ParallelBeamSearchMove(this);
		this.soulRock = new SoulStopOppRockSkill(this);
		this.oppRock = new RootStopOppRockSkill(this);
		this.clincherRock = new ClincherOppRockSkill(this);
		this.badAvatar = new MakeBadOppAVaterSkill(this);
		this.oppAvatar = new RootStopOppAvatarSkill(this);
		this.sword = new RowCostSwordSkill(this);
		this.oppThunder = new DogRootOppThunderSkill(this);
		this.clincherAvatar = new ClincherOppAvatarSkill(this);
	}
	boolean isUseSword = false;
	@Override
	public String calcOrder() {
		res = new StringBuilder();

		String skillRes = "";
		isUseSword = false;
		skillRes = useSkill();

		res.append(ninjaMove(skillRes, isUseSword));
		/*for (int i = 0; i < mine.ninjas.length; ++i) {
			res.append(order(mine.ninjas[i])).append("\n");
		}*/
		return res.toString();
	}
	//スキルを実際に使う。戻り値は使ったかどうか。
	String useSkill() {
		String ret = "";
		//回転斬りが一定以上のときのみ攻撃
		boolean noAttack = Main.skillCost[Main.SWORD_SKILL] <= NO_ATTACK_SWORD_COST && opp.point >= Main.skillCost[Main.SWORD_SKILL];


		//回転斬り
		if (ret.isEmpty()) {
			ret = sword.useSkill();
			if (!ret.isEmpty()) {
				isUseSword = true;//使ったらフラグオン
			}
		}

		//止め敵分身スキル
		/*if (ret.isEmpty()) {
			ret = clincherAvatar.useSkill();
		}*/

		if (!noAttack) {
			//止め落石スキル
			if (ret.isEmpty()) {
				ret = clincherRock.useSkill();
			}
			//追い詰め敵落石スキル
			if (ret.isEmpty()) {
				ret = oppRock.useSkill();
			}
			//追い詰め敵分身
			if (ret.isEmpty()) {
				ret = oppAvatar.useSkill();
			}
		}
		//Soul止め敵落石
		/*if (ret.isEmpty()) {
			ret = soulRock.useSkill();
		}


		//引っ掛け敵分身
		if (ret.isEmpty()) {
			ret = badAvatar.useSkill();
		}*/

		//敵雷動かないから消す
		/*if (ret.isEmpty()) {
			ret = oppThunder.useSkill();
		}*/

		//分身スキル使えるなら

		//行数計算
		if (ret.isEmpty()) {
			ret = "2\n";
		}else {
			ret = "3\n" + ret;
		}
		return ret;
	}



	//全探索忍者移動
	String ninjaMove(String skillRes, boolean isUseSword) {
		return move.ninjaMove(skillRes, isUseSword);
	}

}
