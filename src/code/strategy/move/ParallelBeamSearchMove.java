package code.strategy.move;

import java.awt.Point;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Queue;

import code.Dog;
import code.Main;
import code.MapCalc;
import code.MovedData;
import code.Ninja;
import code.strategy.Strategy;
//並列化してみたやつ
public class ParallelBeamSearchMove extends Move {

	public static final int CALC_TURN = 6;

	public static final int BEAM_WIDTH = 1500;

	public static final double LOCK_ROCK_PRI = -3;

	public static final double THUNDER_PRI = -0.6;
	public static final double MOVE_PRI = 0.1;
	public static final double SPEED_PRI = -0.6;
	public static final double AVATAR_PRI = -1.2;

	public ParallelBeamSearchMove(Strategy strategy) {
		super(strategy);
	}

	@Override
	public String ninjaMove(String skillRes, boolean isUseSword) {
		preSet();
		String ret = "\n\n";
		//double MaxPre = Double.NEGATIVE_INFINITY;
		int moveNum = 2;
		Ninja n1 = mine.ninjas[0];
		Ninja n2 = mine.ninjas[1];
		//初期の優先度
		double fPri = mine.primaryMap[n1.row][n1.col] + mine.primaryMap[n2.row][n2.col];
		Queue<MovedData> list = new PriorityQueue<MovedData>(BEAM_WIDTH+1);
		list.add(new MovedData(new int[0], mine.ninjas, mine.map,mine.dogMap
				,fPri, mine.primaryMap, mine.itemMap, -1, -1, new Point[Main.map_row][Main.map_col]
						, false, -1, -1, isUseSword));
		//分身出せるなら
		if (mine.point >= Main.skillCost[Main.MY_AVATAR_SKILL] && !isUseSword) {
			Point[] aps = getAvatarPos(MapCalc.getAvaPriMap(mine.itemMap, mine.dogMap, mine.map, mine.ninjas), mine.map, mine.ninjas);
			for (Point ap: aps) {
				if (ap.x != -1) {//出す場所あるなら
					//分身使った時の優先度
					double aPri;
					if (!skillRes.equals("2\n")) {//攻撃したなら
						aPri = fPri + AVATAR_PRI*  Main.skillCost[Main.MY_AVATAR_SKILL]*3;

					}else {
						aPri = fPri + AVATAR_PRI*  Main.skillCost[Main.MY_AVATAR_SKILL];
					}
					list.add(new MovedData(new int[0], mine.ninjas, mine.map,mine.dogMap
							,aPri , mine.primaryMap, mine.itemMap, -1, -1, new Point[Main.map_row][Main.map_col]
									, false, ap.x, ap.y, isUseSword));
				}
			}

		}
		//System.err.println("move");



		//移動選択肢列挙
		LOOP:for (int v = 0; v < CALC_TURN; v++) { //Search数
			Queue<MovedData> li = list;
			for (int i=0; i < 2; i++) {//キャラ数
				for (int t=0; t < moveNum; t++) {//移動数
					//System.err.println("list:" +list.size());


					li = calcMoved(i, li, t==moveNum-1,v==0,v, false, !skillRes.equals("2\n"));//犬は移動後だけ、カミナリ打つのは最初だけ

					//System.err.println("list:" +li.size());
				}

			}
			if ( v== 0 && mine.point >= Main.skillCost[Main.SPEED_SKILL] && !isUseSword){
				Queue<MovedData> li2 = list;
				for (int i=0; i < 2; i++) {//キャラ数
					for (int t=0; t < moveNum+1; t++) {//移動数
						//System.err.println("list:" +list.size());

						//超高速も追加

						li2 = calcMoved(i, li2, t == moveNum,v==0,v, t == moveNum,!skillRes.equals("2\n"));


						//System.err.println("list:" +li.size());
					}

				}
				li.addAll(li2);
			}
			if (li.isEmpty()) {//リストが空(死亡確定)ならそこで終了
				break LOOP;
			}
			list = li;

			//幅越えてたらその分だけ消す
			if (list.size() > BEAM_WIDTH) {
				int n = list.size() -BEAM_WIDTH;
				for (int i=0; i < n; i++) {
					list.remove();
				}
			}
			//マップ更新
			updateMap(list,v);
		}
		//移動判定
		MovedData md = list.stream().max((a,b)-> a.compareTo(b)).orElse(null);
		ret = "";
		//カミナリ打ったなら

		if (md.tr != -1) {
			if (md.isUseSpeed) {
				System.err.println("ERR3");
			}
			/*if (mine.map[md.tr][md.tc] != Main.ROCK) {
				System.err.println("ERR");
			}*/
			ret += "3\n" + Main.MY_THUNDER_SKILL +" " + md.tr + " " +md.tc + "\n";
		}else if (md.isUseSpeed) {
			ret += "3\n" + Main.SPEED_SKILL + "\n";
			moveNum = 3;
			System.err.println("useSpeed");
			//分身したなら
		}else if (md.ar != -1) {
			System.err.println("useMyAva");
			ret += "3\n" + Main.MY_AVATAR_SKILL +" " + md.ar + " " +md.ac + "\n";
		}else {
			ret += skillRes;
		}
		int ind = 0;
		for (int i=0; i < 2; i++) {
			for (int t=0; t < moveNum; t++) {
				String s;
				if (ind < md.dirs.length) {
					s = Main.ds[md.dirs[ind]];
				}else {
					s = "";
				}
				ret += s;
				ind++;
			}
			ret += "\n";
		}

		return ret;
	}
	//衝突は他でやったので犬マップの更新
	public void updateMap(Queue<MovedData> list, int turn) {
		list.parallelStream().forEach(md->{
			//データ整理
			Ninja nn1 = md.ninjas[0], nn2 = md.ninjas[1];
			//犬座標計算

			short[][] nextDogMap;

			if( turn == 0 && md.ar != -1) {//分身使ったなら
				nextDogMap = Dog.calcDog(md.ar, md.ac, -1, -1, md.dogMap, md.map);
			}else {
				nextDogMap = Dog.calcDog(nn1.row, nn1.col, nn2.row, nn2.col, md.dogMap, md.map);
			}
			/*if (nextDogMap[nn1.row][nn1.col] != -1 || nextDogMap[nn2.row][nn2.col] != -1 ) {
				it.remove();
			}else {*/
			//はじめのターンのみ
			if (turn == 0) {
				//追加される可能性のある犬の追加
				nextDogMap = Dog.addDog(nn1.row, nn1.col, nn2.row, nn2.col, nextDogMap, md.map,opp.nextItem);

			}
			//犬マップ更新
			md.dogMap = nextDogMap;
			//優先度マップ更新
			md.priorityMap = MapCalc.getPriMap(mine.itemMap, md.dogMap, md.map,md.ninjas, mine.point);
		});
	}
	//動いた後のリストを返すメソッド。犬フラグ立ってたら犬と当たってるなら選択肢から消す。岩フラグなら壊せる
	public Queue<MovedData> calcMoved(int ninjaID, Queue<MovedData> list
			, boolean checkDog, boolean breakRock, int turn,boolean isUseSpeed, boolean isAttack) {//turnはなん手先か
		Queue<MovedData> ret = new PriorityQueue<MovedData>(BEAM_WIDTH+1);
		list.stream().forEach(md->{
			Ninja nin = md.ninjas[ninjaID];
			LOOP:for (int d= 0; d < 5;d++)  {
				//枝刈り(1マスしか移動しない)
				if (checkDog && md.dirs[md.dirs.length-1] != 4 && d == 4) {
					continue;
				}
				//カミナリつかってるならやらない
				if (isUseSpeed && md.tr != -1) {
					continue;
				}
				//分身つかってるならやらない
				if (isUseSpeed && md.ar != -1) {
					continue;
				}
				/*//超高速つかったら２番めも使う
				if (turn == 0 && ninjaID == 1 && md.isUseSpeed && checkDog && !isUseSpeed) {
					continue;
				}
				//超高速つかってないなら使わない
				if (turn == 0 && ninjaID == 1&& !md.isUseSpeed && checkDog && isUseSpeed) {
					continue;
				}*/
				int nextR = nin.row + Main.dx[d], nextC = nin.col + Main.dy[d];
				//他のスキルつかってるなら使えない
				boolean canUseThunder = breakRock && !isUseSpeed && !md.isUseSpeed && md.ar ==-1 && !md.isUseSword;
				Point thunderPos = null;
				//動く先が壁なら
				if (md.map[nextR][nextC] == Main.WALL) {
					//やり直し
					continue;
					//動かせない石で
				}else if(md.map[nextR][nextC] == Main.ROCK
						&& !MapCalc.isMoveStone(nextR, nextC, d, md.map, md.dogMap, md.ninjas)) {
					//まだカミナリ使ってなくてカミナリ使えるなら
					if (canUseThunder&&  md.tr == -1 && mine.point >= Main.skillCost[Main.MY_THUNDER_SKILL]) {
						//後半壊さないようにする
						canUseThunder = false;
						//カミナリ打つの設定
						thunderPos = new Point(nextR, nextC);
					}else {
						//使わないならやめる
						continue;
					}
				}



				//移動後の忍者とマップ
				Ninja nextNin = (Ninja)nin.clone();
				nextNin.row += Main.dx[d];
				nextNin.col += Main.dy[d];
				//移動後に犬の近くならやめる
				if (checkDog) {
					if (turn == 0 && md.ar != -1) {//分身つかったはじめのターンなら
						//TODO 移動マップでてからやる

					}else{
						for (int i=0; i < 5; i++) {
							int nr = nextNin.row + Main.dx[i], nc = nextNin.col + Main.dy[i];
							if (md.dogMap[nr][nc] != -1) {
								continue LOOP;
							}
						}
					}
				}
				double pri = md.pri;
				//前に移動できてなかったのなら止められる心配はない
				if (thunderPos == null) {
					//雷を打つポイント
					thunderPos = checkRock(canUseThunder, md, nin, d, nextNin, pri);
					//打たなきゃいけなくて
					if (thunderPos != null) {
						//雷打たないなら優先度下げる
						if (thunderPos.x == -1) {
							pri += LOCK_ROCK_PRI;
							thunderPos.move(md.tr, md.tc);
						}else {
							pri += THUNDER_PRI*Main.skillCost[Main.MY_THUNDER_SKILL];
							//カミナリうってもちょっとだけ優先度下げる。
						}
					}else {
						//前の入れる
						thunderPos = new Point(md.tr,md.tc);
					}
				}else {//打つなら優先度下げる
					pri += THUNDER_PRI*Main.skillCost[Main.MY_THUNDER_SKILL];
				}
				//超高速使ったら優先度下げる
				if (isUseSpeed) {
					if (isAttack) {//攻撃してたらかなり優先度下げる
						pri += SPEED_PRI * Main.skillCost[Main.SPEED_SKILL] *Main.skillCost[Main.SPEED_SKILL]*10;
					}else {
						pri += SPEED_PRI * Main.skillCost[Main.SPEED_SKILL] *Main.skillCost[Main.SPEED_SKILL];
					}
				}

				//道のりの優先度も足す
				if (d != 4) {
					//超高速じゃなくて動くなら優先度足す
					if (!isUseSpeed) {
						pri += MOVE_PRI;
					}
					//アイテムがあるなら足す
					if (md.itemMap[nextNin.row][nextNin.col])
						pri += mine.primaryMap[nextNin.row][nextNin.col]*(CALC_TURN);
					else if (mine.primaryMap[nextNin.row][nextNin.col] < MapCalc.ITEM_PRE){
						//アイテムがなくてアイテムあった場所じゃなければ
						pri += mine.primaryMap[nextNin.row][nextNin.col]*(CALC_TURN);
					}
					//通ったところの優先度を低くする
					//nextPriMap[nextNin.id][nextNin.row][nextNin.col] *= 0.25;//4分の1
				}


				Ninja[] nextNinjas;
				if (ninjaID == 0) {
					nextNinjas = new Ninja[]{nextNin, md.ninjas[1]};
				}else {
					nextNinjas = new Ninja[]{md.ninjas[0], nextNin};
				}
				//カミナリ落としたなら
				char[][] nowMap = md.map;
				if (md.tr == -1 && thunderPos.x != -1) {
					//床にする
					nowMap = MapCalc.copyMap(nowMap);
					if (nowMap[thunderPos.x][thunderPos.y] != Main.ROCK) {
						System.err.println("ERR");
					}
					nowMap[thunderPos.x][thunderPos.y] = Main.FLOOR;
					Point cp = md.rockMoveMap[thunderPos.x][thunderPos.y] ;
					//動かされてたらカミナリの出す場所を移動
					if(cp != null){
						thunderPos.move(cp.x, cp.y);
					}
				}
				Point[][] nextMoveMap = md.rockMoveMap;
				//岩が動くなら
				if (MapCalc.isMoveStone(nextNin.row, nextNin.col, d, nowMap, md.dogMap, md.ninjas)) {
					nextMoveMap = MapCalc.copyMap(md.rockMoveMap);
					int nr = nextNin.row + Main.dx[d], nc = nextNin.col + Main.dy[d];
					//移動もとの座標入れる
					if (nextMoveMap[nextNin.row][nextNin.col] == null) {
						nextMoveMap[nr][nc] = new Point(nextNin.row, nextNin.col);
					}else {
						//前の座標があるなら前の
						nextMoveMap[nr][nc] = nextMoveMap[nextNin.row][nextNin.col];
						nextMoveMap[nextNin.row][nextNin.col] = null;
					}


				}
				char[][] nextMap = MapCalc.getMovedMap(nin.row, nin.col, d, nowMap, md.dogMap, nextNinjas);

				//分身ありの時の接触判定
				if (checkDog) {
					if (turn == 0 && md.ar != -1) {//分身つかったはじめのターンなら
						short[][] avaDogMap = Dog.calcDog(md.ar, md.ac, -1, -1, md.dogMap,nextMap);
						//犬と重なったならやめる
						if (avaDogMap[nextNin.row][nextNin.col] != -1) {
							continue;
						}
					}
				}


				//移動方向追加
				int[] nextD = Arrays.copyOf(md.dirs, md.dirs.length+1);
				nextD[md.dirs.length] = d;
				boolean[][] nextItemMap;
				//アイテムと接触してたらアイテム消したマップにする
				if (md.itemMap[nextNin.row][nextNin.col]) {
					nextItemMap = MapCalc.copyMap(md.itemMap);
					nextItemMap[nextNin.row][nextNin.col] = false;
				}else {
					nextItemMap = md.itemMap;
				}
				//登録
				ret.add(new MovedData(nextD, nextNinjas, nextMap,md.dogMap, pri,md.priorityMap,nextItemMap
						, thunderPos.x, thunderPos.y, nextMoveMap,md.isUseSpeed || isUseSpeed,md.ar, md.ac, md.isUseSword));
				//幅越えてたらその分だけ消す
				/*if (ret.size() > BEAM_WIDTH) {
					ret.remove();
				}*/
			}
		});
		return ret;
	}
	//敵の落石の考慮 必要ないならnull、雷打たないなら-1の座標持ったやつ返す。
	private Point checkRock(boolean breakRock, MovedData md, Ninja nin, int d, Ninja nextNin, double pri) {
		//岩を動かして移動してて移動前に犬の近くならできるだけやめるか雷落とす
		if (opp.point >= Main.skillCost[Main.OPP_ROCK_SKILL]) {//敵が発動できて
			//移動先が岩かその先が岩か壁か犬か忍者で
			int dr = nextNin.row + Main.dx[d], dc = nextNin.col + Main.dy[d];
			if (md.map[nextNin.row][nextNin.col] == Main.ROCK
					||( !mine.ninjas[0].equalPos(nextNin.row, nextNin.col) && !mine.ninjas[1].equalPos(nextNin.row, nextNin.col)
							&&isRockStopper(dr, dc, md.map, md.dogMap, md.ninjas))) {//岩置かれるところが初期位置なら考慮しない
				//壊したい岩の座標
				int sr = -1, sc = -1;
				//止めるのが岩なら
				if (md.map[nextNin.row][nextNin.col] == Main.ROCK ){
					sr = nextNin.row;
					sc = nextNin.col;
				}
				else if(md.map[dr][dc] == Main.ROCK) {
					sr = dr;
					sc = dc;
				}
				//移動前が犬の近くなら
				for (int i=0; i < 5; i++) {
					int nr = nin.row + Main.dx[i], nc = nin.col + Main.dy[i];
					if (md.dogMap[nr][nc] != -1) {
						//石が邪魔してまだ雷使ってなくて雷つかえるなら
						if (breakRock && sr != -1 && md.tr == -1 && mine.point >= Main.skillCost[Main.MY_THUNDER_SKILL]) {
							//雷打つ
							return new Point(sr,sc);
						}else {
							//優先度をかなりマイナス(一応落とされなくて逃げられる場合も考慮)
							return new Point(-1,-1);
						}
					}
				}
			}
		}
		return null;
	}
	//分身を出す座標追加
	Point[] getAvatarPos(float[][] priMap, char[][] map, Ninja[] ninjas) {
		Point[] ret = new Point[2];
		float min = Float.MAX_VALUE;
		int ar = -1, ac = -1;
		//とりあえず一番低い値
		for (int r = 0; r < Main.map_row; r++) {
			for (int c = 0; c < Main.map_col; c++) {
				if (min > priMap[r][c] && map[r][c] == Main.FLOOR && priMap[r][c] !=  0) {
					min = priMap[r][c];
					ar = r;
					ac = c;
				}

			}
		}
		ret[0] = new Point(ar, ac);
		//距離マップ
		int max = 0;
		ar = -1;
		ac = -1;
		int[][] distMap = MapCalc.getDistMap(ninjas[0].row, ninjas[0].col, ninjas[1].row, ninjas[1].col, map);
		for (int r = 0; r < Main.map_row; r++) {
			for (int c = 0; c < Main.map_col; c++) {
				if (max < distMap[r][c] && map[r][c] == Main.FLOOR && distMap[r][c] != MapCalc.MAX_INT) {
					max = distMap[r][c];
					ar = r;
					ac = c;
				}

			}
		}
		ret[1] = new Point(ar, ac);
		return ret;
	}
	//岩を止める要素か確認
	boolean isRockStopper(int r, int c,char[][] map, short[][] dogMap, Ninja[] ninjas) {
		//岩か壁か犬か忍者で
		return map[r][c] != Main.FLOOR || dogMap[r][c] != -1
				|| ninjas[1].equalPos(r, c);//忍者は最初に動くやつだけ考慮
	}
	//忍者移動の事前準備
	void preSet() {
		mine.primaryMap = MapCalc.getPriMap(mine.itemMap, mine.dogMap, mine.map,mine.ninjas, mine.point);
	}
}
