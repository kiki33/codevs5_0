package code.strategy.move;

import code.PlayerData;
import code.strategy.Strategy;

public abstract class Move {
	//戦略
	public Strategy strategy;

	public PlayerData mine;
	public PlayerData opp;

	public Move(Strategy strategy) {
		this.strategy = strategy;
		this.mine = strategy.mine;
		this.opp = strategy.opp;
	}

	//忍者移動の文字を返す
	public abstract String ninjaMove(String skillRes, boolean isUseSword);

}
