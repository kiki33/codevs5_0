package code.strategy.skill;

import code.Main;
import code.Ninja;
import code.strategy.Strategy;
//コストが低い時に回転斬り
public class RowCostSwordSkill extends SkillDecision {
	public static final int MAX_COST = 8;

	static final int USE_DOG_NUM = 2;

	public RowCostSwordSkill(Strategy strategy) {
		super(strategy);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public String useSkill() {
		//コストが低くて使えるなら
		if ( mine.point >= Main.skillCost[Main.SWORD_SKILL]) {
			for (int i=0; i < 2; i++) {
				int dogNum = 0;
				Ninja n = mine.ninjas[i];
				//犬を数える
				for (int x=-1; x <= 1; x++) {
					for (int y=-1; y <= 1; y++) {
						int nr = n.row + x, nc = n.col + y;
						if(mine.dogMap[nr][nc] != -1) {
							dogNum++;
						}
					}
				}
				//System.err.println(i+":"+dogNum);

				int useDog;
				//数によって比例させる
				if (Main.skillCost[Main.SWORD_SKILL] <= MAX_COST ) {
					useDog = USE_DOG_NUM;//コスト以内ならそのまま
				}else {
					useDog = USE_DOG_NUM + (Main.skillCost[Main.SWORD_SKILL] - MAX_COST+1)/2;

				}

				//犬が近くに沢山いるなら
				if (dogNum >= useDog) {
					//犬マップから消しておく
					//犬マップから消しておく
					for (int x=-1; x <= 1; x++) {
						for (int y=-1; y <= 1; y++) {
							int nr = n.row + x, nc = n.col + y;
							mine.dogMap[nr][nc] = -1;
						}
					}
					return Main.SWORD_SKILL+ " "+i+ "\n";
				}
			}
		}
		return "";
	}

}
