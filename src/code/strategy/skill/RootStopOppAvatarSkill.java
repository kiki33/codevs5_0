package code.strategy.skill;

import code.Dog;
import code.Main;
import code.MapCalc;
import code.Ninja;
import code.PointsAndNum;
import code.strategy.Strategy;

public class RootStopOppAvatarSkill extends SkillDecision {
	public static final int USE_DIST = 2;

	public boolean befUse = false;
	public RootStopOppAvatarSkill(Strategy strategy) {
		super(strategy);
	}

	@Override
	public String useSkill() {
		if (befUse) {
			System.err.println("befUse");
		}
		//分身使ってて分身使えるなら
		if (opp.useSkill[Main.MY_AVATAR_SKILL] > 0 && opp.point >= Main.skillCost[Main.MY_AVATAR_SKILL] /*&& !befUse*/) {
			for (int i=0; i < 2; i++) {
				Ninja nin = opp.ninjas[i];
				PointsAndNum pn = MapCalc.getMoveMapNum(nin.row, nin.col, opp.map, opp.dogMap, opp.ninjas, opp.itemMap);
				//移動可能場所がなくて

				int stopNum = pn.points.size();

				if (stopNum == 0) {
					//移動可能範囲が犬の数の半分以下で分身を出せそうなら
					if (pn.num <= opp.dogList.length && mine.point >= Main.skillCost[Main.OPP_AVATAR_SKILL]) {

						System.err.println("useAva");
						//出す
						int[][] distMap = MapCalc.getDistMap(nin.row, nin.col, -1, -1, opp.map);
						int maxNum = 0;
						int pr = -1, pc = -1;
						LOOP:for (Dog d:opp.dogList) {
							//意表をつくため逆順
							for (int t=4; t >= 0; t--) {
								int nr = d.row +Main.dx[t], nc = d.col +Main.dy[t];
								int dist = distMap[nr][nc];
								//範囲以内で
								if (dist <= USE_DIST) {

									short[][] nextDogMap = Dog.calcDog(nr, nc, -1, -1, opp.dogMap, opp.map);
									//犬マップを調べる
									int num = 0;
									for (int r = 0; r < Main.map_row; r++) {
										for (int c = 0; c < Main.map_col; c++) {
											//移動可能範囲で犬が埋まってれば
											if (distMap[r][c] <= 2 && nextDogMap[r][c] != -1) {
												num++;
											}
										}
									}
									if (num > maxNum) {
										maxNum = num;
										pr = nr;
										pc = nc;
									}


								}

							}

						}
						if (pr != -1) {
							befUse = true;
							return Main.OPP_AVATAR_SKILL+ " "+pr+" "+ pc+ "\n";
						}
					}
				}
			}
		}
		befUse = false;
		return "";
	}

}
