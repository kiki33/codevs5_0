package code.strategy.skill;

import java.awt.Point;

import code.Main;
import code.MapCalc;
import code.Ninja;
import code.PointsAndNum;
import code.strategy.Strategy;
//道を塞ぐ敵落石
public class RootStopOppRockSkill extends SkillDecision {
	public static final int STOP_ROOT_NUM = 2;
	public RootStopOppRockSkill(Strategy strategy) {
		super(strategy);
	}

	@Override
	public String useSkill() {
		if (Main.skillCost[Main.OPP_ROCK_SKILL] >=Main.skillCost[Main.MY_THUNDER_SKILL]  && opp.point >= Main.skillCost[Main.MY_THUNDER_SKILL] ) {
			return "";
		}
		for (int i=0; i < 2; i++) {
			Ninja nin = opp.ninjas[i];
			PointsAndNum pn = MapCalc.getMoveMapNum(nin.row, nin.col, opp.map, opp.dogMap, opp.ninjas, opp.itemMap);
			//止める場所があって

			int stopNum = pn.points.size();

			if (stopNum != 0 && stopNum <= STOP_ROOT_NUM) {
				//移動可能範囲が犬の数の半分以下で岩を止める数打てるなら
				if (pn.num <= opp.dogList.length && mine.point >= stopNum* Main.skillCost[Main.OPP_ROCK_SKILL]) {
					int min = 10000,pr = -1, pc = -1;
					//距離が小さいほうから埋める
					for (Point p: pn.points) {
						int  dist = Main.getDist(nin.row, nin.col, p.x, p.y);
						if (dist < min) {
							min = dist;
							pr = p.x;
							pc = p.y;
						}
					}
					//System.err.println("useRock");
					//落とす
					 return Main.OPP_ROCK_SKILL+ " "+pr+" "+ pc+ "\n";
				}
			}
		}


		return "";
	}

}
