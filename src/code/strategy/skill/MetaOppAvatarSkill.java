package code.strategy.skill;

import code.Main;
import code.MapCalc;
import code.Ninja;
import code.strategy.Strategy;
//相手の分身メタる敵分身
public class MetaOppAvatarSkill extends SkillDecision {

	//分身を使う時の犬との距離
	public static final int AVATAR_SKILL_DOG_DIST = 3;
	//分身を使う時のアイテムとのちょうどの距離
	public static final int AVATAR_SKILL_ITEM_DIST = 2;

	public MetaOppAvatarSkill(Strategy strategy) {
		super(strategy);
	}

	@Override
	public String useSkill() {
		//分身スキル使えるなら
		if ( Main.skillCost[Main.OPP_AVATAR_SKILL] <= mine.point) {
			//出す座標

			//敵が以前に分身使ってて分身使えるコストあるなら
			if (opp.useSkill[Main.MY_AVATAR_SKILL] > 0 && opp.point >= Main.skillCost[Main.MY_AVATAR_SKILL]) {
				//犬が近くにある近いアイテムに分身出す
				Ninja[] ns = opp.ninjas;
				int[][] distMap = MapCalc.getDistMap(ns[0].row, ns[0].col, ns[1].row, ns[1].col, opp.map);;
				int minNin = 0;
				int minNinDist = 10000;
				for (int r = 0; r < Main.map_row; r++) {
					for (int c = 0; c < Main.map_col; c++) {

						//アイテムがあって距離が一定以下
						if (opp.itemMap[r][c] && distMap[r][c] == AVATAR_SKILL_ITEM_DIST) {
							//犬が隣なら
							for (int i=0; i < 5; i++) {
								int nr = r + Main.dx[i], nc = c + Main.dy[i];

								if (opp.dogMap[nr][nc] != -1) {
									return Main.OPP_AVATAR_SKILL+ " "+r+" "+ c+ "\n";

								}
							}
						}
					}
				}


			}

		}
		return "";
	}

}
