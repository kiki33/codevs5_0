package code.strategy.skill;

import java.awt.Point;
import java.util.ArrayList;

import code.Dog;
import code.Main;
import code.MapCalc;
import code.Ninja;
import code.strategy.Strategy;
//悪い挙動にさせる敵分身
public class MakeBadOppAVaterSkill extends SkillDecision {
	//以前の場所
	public int befR = -1, befC = -1;

	public MakeBadOppAVaterSkill(Strategy strategy) {
		super(strategy);
	}

	@Override
	public String useSkill() {
		//分身スキル使えるなら
		if ( Main.skillCost[Main.OPP_AVATAR_SKILL] <= mine.point) {
			//出す座標

			//敵が以前に分身使ってて分身使えるコストあってスキル無しじゃSoul取れないなら
			if (opp.useSkill[Main.MY_AVATAR_SKILL] > 0 && opp.point >= Main.skillCost[Main.MY_AVATAR_SKILL]
					&& !isCanSoul(opp.itemMap, opp.dogMap, opp.map, opp.ninjas)) {
				//犬が近くにある近いアイテムに分身出す
				Ninja[] ns = opp.ninjas;

				float[][] priMap = MapCalc.getOppPriMap(opp.itemMap, opp.dogMap, opp.map, opp.ninjas);
				//int[][] distMap =  MapCalc.getDistMap(ns[0].row, ns[0].col, ns[1].row, ns[1].col, opp.map);
				float maxPri = -1;
				int pr = -1, pc = -1;
				for (int id = 0; id < 2; id++) {
					for (int r = 0; r < Main.map_row; r++) {
						for (int c = 0; c < Main.map_col; c++) {
							int dist = Main.getDist(r, c, ns[id].row, ns[id].col);
							//距離が一定以内
							if (dist <= 2) {
								float pri = priMap[r][c];
								//優先度更新
								if (maxPri < pri) {
									maxPri = pri;
									//犬が隣で距離２なら
									if (Dog.isNearDog(r, c, opp.dogMap) && dist == 2) {
										//分身出す価値あり
										pr = r;
										pc = c;
									}else {
										pr = -1;
										pc = -1;
									}
								}
							}
						}
					}
					if (pr != -1) {
						if (befR == pr &&  befC == pc) {
							System.err.println("repeat");
							//連続ならださない
							befR = -1;
							befC = -1;
						}else {
							//分身出す
							System.err.println("useBadAva");
							//以前の値を保存
							befR = pr;
							befC = pc;
							return Main.OPP_AVATAR_SKILL+ " "+pr+" "+ pc+ "\n";
						}
					}
				}


			}

		}
		//連続のときのみ
		befR = -1;
		befC = -1;
		return "";
	}
	//スキル無しでソウル取れるか判定
	public static boolean isCanSoul(boolean[][] itemMap, short[][] dogMap, char[][] map,Ninja[] ninjas) {
		for (int r=0; r < Main.map_row; r++) {
			for (int c = 0; c < Main.map_col; c++) {
				//アイテムのあるマスを優先していけるようにする。ただし犬と被ってるなら除外
				if (itemMap[r][c] && dogMap[r][c] == -1 && !Dog.isNearDog(r, c, dogMap)) {
					double[][] pMap = new double[Main.map_row][Main.map_col];
					ArrayList<Point> list = new ArrayList<Point>();
					list.add(new Point(r,c));
					int index = 1;
					pMap[r][c] = MapCalc.ITEM_PRE;
					while (!list.isEmpty()) {
						index++;
						if (index >= MapCalc.ITEM_MAX_INDEX) {
							break;
						}
						ArrayList<Point> nlist = new ArrayList<Point>();
						for (Point p: list) {
							//pMap[p.x][p.y] = Main.ITEM_PRE/index;
							for (int i=0; i < 4; i++) {
								int nr = p.x + Main.dx[i], nc = p.y + Main.dy[i];

								if (Main.isIn(nr, nc)  &&pMap[nr][nc] == 0) {
									//にんじゃと同じ座標なら
									if (ninjas[0].equalPos(nr, nc) || ninjas[1].equalPos(nr, nc)) {
										//アイテム取れるとする
										return true;
									}
									//犬の近くなら通せんぼ
									if (map[nr][nc] == Main.FLOOR && !Dog.isNearDog(nr, nc, dogMap)) {
										pMap[nr][nc] = MapCalc.ITEM_PRE/(index*10);
										//System.err.println(nr+","+nc);
										nlist.add(new Point(nr, nc));
									}
								}
							}
						}
						list = nlist;

					}
				}

			}

		}
		return false;
	}

}
