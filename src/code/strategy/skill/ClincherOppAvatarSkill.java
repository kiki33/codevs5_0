package code.strategy.skill;

import code.Dog;
import code.Main;
import code.Ninja;
import code.strategy.Strategy;
//相手が分身でにげられないようにする敵分身
public class ClincherOppAvatarSkill extends SkillDecision {

	public ClincherOppAvatarSkill(Strategy strategy) {
		super(strategy);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public String useSkill() {

		//敵分身使えて
		if ( Main.skillCost[Main.OPP_AVATAR_SKILL] <= mine.point) {
			for (int i=0; i < 2; i++) {
				Ninja nin = opp.ninjas[i];
				//移動可能方向が０で犬の近くなら
				if (getCanMove(nin.row, nin.col, opp.map, opp.dogMap, opp.ninjas) == 0
						&& Dog.isNearDog(nin.row, nin.col, opp.dogMap)) {
					//敵の場所に分身
					return Main.OPP_AVATAR_SKILL+ " "+nin.row+" "+ nin.col+ "\n";
				}
			}
		}

		return "";
	}

}
