package code.strategy.skill;

import code.Main;
import code.MapCalc;
import code.Ninja;
import code.strategy.Strategy;
//Soul取得を止める岩
public class SoulStopOppRockSkill extends SkillDecision {

	public static final int STOP_DIST = 2;


	public SoulStopOppRockSkill(Strategy strategy) {
		super(strategy);
	}

	@Override
	public String useSkill() {
		//岩スキル使えるなら
		if ( Main.skillCost[Main.OPP_ROCK_SKILL]*2 <= mine.point) {
			for (int id = 0; id < 2; id++) {
				Ninja n = opp.ninjas[id];

				//４方向にまっすぐ調べる
				for (int i=0; i < 4; i++) {
					int nr = n.row + Main.dx[i], nc = n.col + Main.dy[i];
					for (int t = 0; t < STOP_DIST -1; t++) {//1はすでに足されてる
						int nnr = nr + Main.dx[i], nnc = nc + Main.dy[i];

						if (Main.isIn(nnr, nnc)) {
							//先がアイテムでその間に岩がおけて
							if (opp.itemMap[nnr][nnc] && MapCalc.canPutRock(nr, nc, opp.map, opp.dogMap, opp.itemMap, opp.ninjas)) {
								int wr = nnr + Main.dx[i], wc = nnc + Main.dy[i];
								//アイテムの先が壁かいわなら
								if (opp.map[wr][wc] != Main.FLOOR) {
									//岩を置く
									System.err.println("soulRock");
									return Main.OPP_ROCK_SKILL+ " "+nr+" "+ nc+ "\n";
								}
							}
						}else {//中じゃないならこの方向は抜ける
							break;
						}

						nr = nnr;
						nc = nnc;
					}
				}
			}
		}
		return "";
	}

}
