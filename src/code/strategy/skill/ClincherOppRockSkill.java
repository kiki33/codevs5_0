package code.strategy.skill;

import code.Dog;
import code.Main;
import code.MapCalc;
import code.Ninja;
import code.strategy.Strategy;
//自分の行動止める敵落石
public class ClincherOppRockSkill extends SkillDecision {
	//岩を落とす時の犬との距離
	public static final int ROCK_SKILL_DIST = 1;
	//岩を落とす可能方向数
	public static final int ROCK_SKILL_DIR_NUM = 1;


	public ClincherOppRockSkill(Strategy strategy) {
		super(strategy);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public String useSkill() {
		String ret =  "";
		//落石スキル使えるなら
		if (Main.skillCost[Main.OPP_ROCK_SKILL] <= mine.point) {
			//敵の近くに犬がいたら使う
			boolean[] nd = new boolean[2];
			for (int i=0; i < 2; i++) {
				int minDist = 10000;
				Ninja nin = opp.ninjas[i];
				for (Dog d: opp.dogList) {
					minDist = Math.min(minDist, Main.getDist(nin.row, nin.col, d.row, d.col));
				}
				if (minDist <= ROCK_SKILL_DIST) {
					nd[i] = true;
				}
			}


			if (nd[0]) {
				Ninja nin = opp.ninjas[0];
				int canD = getCanMove(nin.row, nin.col, opp.map, opp.dogMap, opp.ninjas);
				//可能方向が一定以下なら
				if(canD <= ROCK_SKILL_DIR_NUM) {
					ret = putOppRockMsd( nin);
				}
			}
			if (ret.isEmpty() &&  nd[1] ) {
				Ninja nin = opp.ninjas[1];
				int canD = getCanMove(nin.row, nin.col, opp.map, opp.dogMap, opp.ninjas);
				//可能方向が一定以下なら
				if(canD <= ROCK_SKILL_DIR_NUM) {
					ret = putOppRockMsd( nin);
				}
			}

		}
		return ret;
	}
	public String putOppRockMsd( Ninja nin) {
		for (int i=0; i < 4; i++) {
			int nr = nin.row + Main.dx[i], nc = nin.col + Main.dy[i];
			int nnr = nr + Main.dx[i], nnc = nc+ Main.dy[i];
			//2つ先に岩が置けて
			if (Main.isIn(nnr, nnc) && MapCalc.canPutRock(nnr, nnc, opp.map, opp.dogMap, opp.itemMap, opp.ninjas)) {
				//その手前が岩なら
				if (MapCalc.isRock(nr, nc, opp.map)) {
					//そこに置く
					return Main.OPP_ROCK_SKILL+ " "+nnr+" "+ nnc+ "\n";

				}
			}

			//岩が置けて
			if (MapCalc.canPutRock(nr, nc, opp.map, opp.dogMap, opp.itemMap, opp.ninjas)) {
				//動かせるか計算
				char[][] nextMap = MapCalc.copyMap(opp.map);
				nextMap[nr][nc] = Main.ROCK;

				//動かせなくて犬の道遮らないなら
				if (!MapCalc.isMoveStone(nr, nc, i, nextMap, opp.dogMap, opp.ninjas)
						&& (opp.map[nnr][nnc] != Main.WALL  && opp.dogMap[nnr][nnc] == -1)) {//壁じゃないかつ犬じゃない
					//動かせないならそこに置く
					return Main.OPP_ROCK_SKILL+ " "+nr+" "+ nc+ "\n";


				}
			}
		}
		return "";
	}

}
