package code.strategy.skill;

import code.Main;
import code.MapCalc;
import code.Ninja;
import code.PlayerData;
import code.strategy.Strategy;

//スキル使うか決定するやつ
public abstract class SkillDecision {

	//戦略
	public Strategy strategy;

	public PlayerData mine;
	public PlayerData opp;

	public SkillDecision(Strategy strategy) {
		this.strategy = strategy;
		this.mine = strategy.mine;
		this.opp = strategy.opp;
	}
	//スキルを使う。
	public abstract String useSkill();


	//動ける方向の数を返す。死ぬ場合も動けない
	public int getCanMove(int r, int c, char[][] map, short[][] dogMap, Ninja[] ninjas) {
		int ret = 0;
		for (int i=0; i < 4; i++) {
			int nr = r + Main.dx[i], nc = c + Main.dy[i];
			//犬のいない床か動かせる石なら
			if ((map[nr][nc] == Main.FLOOR && dogMap[nr][nc] == -1)
					||(MapCalc.isMoveStone(nr, nc, i, map, dogMap, ninjas))){
				//動かせるに追加
				ret++;
			}
		}
		return ret;
	}
}
