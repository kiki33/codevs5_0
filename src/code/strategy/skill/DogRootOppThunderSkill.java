package code.strategy.skill;

import code.Main;
import code.MapCalc;
import code.Ninja;
import code.strategy.Strategy;

//敵の犬用の道を開ける雷
public class DogRootOppThunderSkill extends SkillDecision {
	public static final int THUNDER_SKILL_DIFF = 9;
	public static final int THUNDER_SKILL_DIST = 1;
	public static final int DOG_NUM = 16;//これ以上犬がいる時打つ

	public DogRootOppThunderSkill(Strategy strategy) {
		super(strategy);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public String useSkill() {
		//スキル使えて犬が一定以上
		if (mine.point >= Main.skillCost[Main.OPP_THUNDER_SKILL] && opp.dogList.length >= DOG_NUM) {
			//距離マップ
			Ninja[] ns = opp.ninjas;
			int[][] distMap = MapCalc.getDistMap(ns[0].row, ns[0].col, ns[1].row, ns[1].col, opp.map);
			//差異の最大値
			int maxDif = 0;
			int pr = -1, pc = -1;
			for (int r = 0; r < Main.map_row; r++) {
				for (int c = 0; c < Main.map_col; c++) {
					//犬なら
					if (opp.dogMap[r][c] != -1) {
						for (int i=0; i < 4; i++) {
							int nr = r + Main.dx[i], nc = c + Main.dy[i];
							//隣が岩でその岩と忍者の距離が一定以下なら
							if (opp.map[nr][nc] == Main.ROCK
									&&( Main.getDist(nr, nc, opp.ninjas[0].row ,opp.ninjas[0].col) <=THUNDER_SKILL_DIST
									|| Main.getDist(nr, nc, opp.ninjas[1].row ,opp.ninjas[1].col) <=THUNDER_SKILL_DIST)) {
								int dogDist = distMap[r][c];
								//どちらかの距離が
								for (int t=0; t < 4; t++) {
									int nnr = r + Main.dx[t], nnc = c + Main.dy[t];
									int nextDist = distMap[nnr][nnc];
									int dif = dogDist - nextDist;
									if (dif > maxDif) {
										maxDif = dif;
										pr = nr;
										pc = nc;
									}
								}
							}
						}
					}

				}
			}
			//差異が一定以上なら使う
			if (THUNDER_SKILL_DIFF <= maxDif) {
				System.err.println("useTha");
				return Main.OPP_THUNDER_SKILL+ " "+pr+" "+ pc+ "\n";
			}
		}

		return "";
	}

}
