package code.strategy;

import code.PlayerData;

public class NoStrategy extends Strategy {

	public NoStrategy(PlayerData mine, PlayerData opp) {
		super(mine, opp);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public String calcOrder() {

		return "2\n\n\n";
	}

}
