package code;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

//マップ上の計算をするクラス
public class MapCalc {
	public static final int MAX_INT = 1000;
	public static final int ITEM_MAX_INDEX = 20;
	static final int DOG_MAX_INDEX = 20;

	public static final double ITEM_PRE = 1.0;
	public static final double DOG_PRE = -0.05;
	//移動後の岩移動を考慮したマップを返す
	//r, c, d キャラの座標と移動方向
	public static char[][] getMovedMap(int r, int c, int d,char[][] map,short[][] dogMap, Ninja[] ninjas) {
		int nr = r + Main.dx[d], nc = c + Main.dy[d];

		char[][] copy;
		if (isMoveStone(nr, nc, d, map, dogMap, ninjas)) {
			copy = copyMap(map);
			//岩を動かす
			int nnr = nr + Main.dx[d], nnc =nc + Main.dy[d];
			copy[nr][nc] = Main.FLOOR;
			copy[nnr][nnc] = Main.ROCK;
		}else {
			//動かないなら参照自体もそのまま
			copy = map;
		}
		//岩が動かないならそのまま
		return copy;
	}

	//その方向から移動可能な石かどうか
	public static boolean isMoveStone(int r, int c, int d,char[][] map,short[][] dogMap, Ninja[] ninjas) {
		if (!isRock(r, c, map)) return false;
		int nr = r + Main.dx[d], nc = c + Main.dy[d];
		return Main.isIn(nr, nc) && map[nr][nc] == Main.FLOOR
				&& !ninjas[0].equalPos(nr, nc) && !ninjas[1].equalPos(nr, nc)
				&& dogMap[nr][nc] == -1;
	}
	//岩かどうか
	public static boolean isRock(int r, int c, char[][] map) {
		return map[r][c] == Main.ROCK;
	}
	//岩を考慮してその方向に動けるかどうか
	public static boolean canMove(int r, int c, int d,char[][] map,short[][] dogMap, Ninja[] ninjas) {
		int nr = r + Main.dx[d], nc = c + Main.dy[d];
		return isMoveStone(nr, nc, d, map, dogMap, ninjas) || map[nr][nc] == Main.FLOOR;

	}
	//指定座標に岩が置けるか
	public static boolean canPutRock(int r, int c,char[][] map,short[][] dogMap, boolean[][] itemMap, Ninja[] ninjas)  {
		return map[r][c] == Main.FLOOR && dogMap[r][c] == -1
				&& !itemMap[r][c]&& !ninjas[0].equalPos(r, c) && !ninjas[1].equalPos(r, c);
	}
	//優先度マップ返す
	public static float[][] getPriMap(boolean[][] itemMap, short[][] dogMap, char[][] map,Ninja[] ninjas, int point) {
		float[][] primaryMap = new float[Main.map_row][Main.map_col];
		for (int r=0; r < Main.map_row; r++) {
			for (int c = 0; c < Main.map_col; c++) {
				//アイテムのあるマスを優先していけるようにする。ただし犬と被ってるなら除外
				if (itemMap[r][c]) {
					double[][] pMap = new double[Main.map_row][Main.map_col];
					ArrayList<Point> list = new ArrayList<Point>();
					list.add(new Point(r,c));
					int index = 1;
					double itemPri = MapCalc.ITEM_PRE;
					if (dogMap[r][c] == -1) {
						itemPri/=2;
					}
					pMap[r][c] = itemPri;
					while (!list.isEmpty()) {
						index++;
						if (index >= ITEM_MAX_INDEX) {
							break;
						}
						ArrayList<Point> nlist = new ArrayList<Point>();
						for (Point p: list) {
							//pMap[p.x][p.y] = Main.ITEM_PRE/index;
							for (int i=0; i < 4; i++) {
								int nr = p.x + Main.dx[i], nc = p.y + Main.dy[i];
								//岩は優先度だけ登録
								if (Main.isIn(nr, nc)  &&pMap[nr][nc] == 0) {
									if (map[nr][nc] == Main.FLOOR) {
										pMap[nr][nc] = itemPri/(index*10);
										//System.err.println(nr+","+nc);
										nlist.add(new Point(nr, nc));
									}else if (map[nr][nc] == Main.ROCK) {
										pMap[nr][nc] = itemPri/(index*10);
									}
								}
							}
						}
						list = nlist;

					}

					for (int x=0; x < Main.map_row; x++) {
						for (int y=0; y < Main.map_col; y++) {
							primaryMap[x][y] += pMap[x][y];

						}
					}
				}
				//犬の近くは行かないようにする剣が使えるなら近づく
				if (dogMap[r][c] != -1 && !(Main.isRowSword && point >= Main.skillCost[Main.SWORD_SKILL])) {
					double[][] pMap = new double[Main.map_row][Main.map_col];
					ArrayList<Point> list = new ArrayList<Point>();
					list.add(new Point(r,c));
					int index = 1;
					pMap[r][c] = MapCalc.DOG_PRE/10;
					while (!list.isEmpty()) {
						index++;
						if (index >=DOG_MAX_INDEX) {
							break;
						}
						ArrayList<Point> nlist = new ArrayList<Point>();
						for (Point p: list) {
							//pMap[p.x][p.y] = Main.ITEM_PRE/index;
							for (int i=0; i < 4; i++) {
								int nr = p.x + Main.dx[i], nc = p.y + Main.dy[i];
								//岩は優先度だけ登録
								if (Main.isIn(nr, nc)  &&pMap[nr][nc] == 0) {
									if (map[nr][nc] == Main.FLOOR) {
										pMap[nr][nc] = MapCalc.DOG_PRE/(index*10);
										//System.err.println(nr+","+nc);
										nlist.add(new Point(nr, nc));
									}else if (map[nr][nc] == Main.ROCK) {
										pMap[nr][nc] = MapCalc.DOG_PRE/(index*10);
									}
								}
							}
						}
						list = nlist;

					}

					for (int x=0; x < Main.map_row; x++) {
						for (int y=0; y < Main.map_col; y++) {

							primaryMap[x][y] += pMap[x][y];
						}
					}
				}
			}

		}
		return primaryMap;
	}
	//敵用の優先度マップ返す
	public static float[][] getOppPriMap(boolean[][] itemMap, short[][] dogMap, char[][] map,Ninja[] ninjas) {
		float[][] primaryMap = new float[Main.map_row][Main.map_col];
		for (int r=0; r < Main.map_row; r++) {
			for (int c = 0; c < Main.map_col; c++) {
				//アイテムのあるマスを優先していけるようにする。ただし犬と被ってるなら除外
				if (itemMap[r][c] && dogMap[r][c] == -1) {
					double[][] pMap = new double[Main.map_row][Main.map_col];
					ArrayList<Point> list = new ArrayList<Point>();
					list.add(new Point(r,c));
					int index = 1;
					pMap[r][c] = MapCalc.ITEM_PRE;
					while (!list.isEmpty()) {
						index++;
						if (index >= ITEM_MAX_INDEX) {
							break;
						}
						ArrayList<Point> nlist = new ArrayList<Point>();
						for (Point p: list) {
							//pMap[p.x][p.y] = Main.ITEM_PRE/index;
							for (int i=0; i < 4; i++) {
								int nr = p.x + Main.dx[i], nc = p.y + Main.dy[i];

								if (Main.isIn(nr, nc)  &&pMap[nr][nc] == 0) {
									if (map[nr][nc] == Main.FLOOR) {
										pMap[nr][nc] = MapCalc.ITEM_PRE/(index*10);
										//System.err.println(nr+","+nc);
										nlist.add(new Point(nr, nc));
									}
								}
							}
						}
						list = nlist;

					}

					for (int x=0; x < Main.map_row; x++) {
						for (int y=0; y < Main.map_col; y++) {

							primaryMap[x][y] += pMap[x][y];

						}
					}
				}

			}

		}
		return primaryMap;
	}
	//分身用の優先度マップ返す
		public static float[][] getAvaPriMap(boolean[][] itemMap, short[][] dogMap, char[][] map,Ninja[] ninjas) {
			float[][] primaryMap = new float[Main.map_row][Main.map_col];
			for (int r=0; r < Main.map_row; r++) {
				for (int c = 0; c < Main.map_col; c++) {
					//アイテムのあるマスを優先していけるようにする。
					if (itemMap[r][c]) {
						double[][] pMap = new double[Main.map_row][Main.map_col];
						ArrayList<Point> list = new ArrayList<Point>();
						list.add(new Point(r,c));
						int index = 1;
						pMap[r][c] = MapCalc.ITEM_PRE;
						while (!list.isEmpty()) {
							index++;
							/*
							if (index >= ITEM_MAX_INDEX) {
								break;
							}*/
							ArrayList<Point> nlist = new ArrayList<Point>();
							for (Point p: list) {
								//pMap[p.x][p.y] = Main.ITEM_PRE/index;
								for (int i=0; i < 4; i++) {
									int nr = p.x + Main.dx[i], nc = p.y + Main.dy[i];

									if (Main.isIn(nr, nc)  &&pMap[nr][nc] == 0) {
										if (map[nr][nc] == Main.FLOOR) {
											pMap[nr][nc] = MapCalc.ITEM_PRE/(index*10);
											//System.err.println(nr+","+nc);
											nlist.add(new Point(nr, nc));
										}
									}
								}
							}
							list = nlist;

						}

						for (int x=0; x < Main.map_row; x++) {
							for (int y=0; y < Main.map_col; y++) {

								primaryMap[x][y] += pMap[x][y];

							}
						}
					}

				}

			}
			return primaryMap;
		}

	//岩移動を考慮しない最短距離のマップを返す
	public static int[][] getDistMap(int r1, int c1, int r2, int c2, char[][] map) {
		int[][] distMap = new int[Main.map_row][Main.map_col];
		for (int[] i: distMap) {
			Arrays.fill(i, MAX_INT);
		}
		ArrayList<Point> list = new ArrayList<Point>();
		list.add(new Point(r1,c1));
		distMap[r1][c1] = 0;
		if (r2 != -1) {
			list.add(new Point(r2,c2));
			distMap[r2][c2] = 0;
		}
		int index = 1;
		//System.err.println("dist");
		while (!list.isEmpty()) {
			//if (debug) Main.printMap(distMap);
			ArrayList<Point> nlist = new ArrayList<Point>();

			for (Point p: list) {
				//distMap[p.x][p.y] = index;

				for (int i=0; i < 4; i++) {
					int nr = p.x + Main.dx[i], nc = p.y + Main.dy[i];
					if (Main.isIn(nr, nc) && map[nr][nc]== Main.FLOOR
							&&distMap[nr][nc] == MAX_INT) {
						nlist.add(new Point(nr, nc));
						distMap[nr][nc] = index;

					}
				}
			}
			//System.err.println();
			//後処理
			list = nlist;
			index++;
		}

		return distMap;
	}
	//移動可能範囲の数を返す
	public static PointsAndNum getMoveMapNum(int r, int c, char[][] map , short[][] dogMap
			, Ninja[] ninjas, boolean[][] itemMap) {
		int ret = 0;
		Set<Point> stopList = new HashSet<>();

		//すでに通ったか
		boolean[][] isMap = new boolean[Main.map_row][Main.map_col];
		ArrayList<Point> list = new ArrayList<Point>();
		list.add(new Point(r,c));
		isMap[r][c] = true;
		//System.err.println("dist");
		while (!list.isEmpty()) {
			//if (debug) Main.printMap(distMap);
			ArrayList<Point> nlist = new ArrayList<Point>();

			for (Point p: list) {
				//distMap[p.x][p.y] = index;

				for (int i=0; i < 4; i++) {
					int nr = p.x + Main.dx[i], nc = p.y + Main.dy[i];
					if (!isMap[nr][nc]) {
						//床で犬いないなら
						if ( map[nr][nc]== Main.FLOOR &&dogMap[nr][nc] == -1) {
							Point np = new Point(nr, nc);
							nlist.add(np);
							isMap[nr][nc] = true;
							ret++;
							//石置く場所が移動先なら消す
							stopList.remove(np);
							//岩なら
						}else if(map[nr][nc]== Main.ROCK) {
							//動かせる岩なら
							if (isMoveStone(nr, nc, i, map, dogMap, ninjas)) {
								int nnr = nr + Main.dx[i], nnc = nc + Main.dy[i];
								//移動先が可能範囲外なら
								if (!isMap[nnr][nnc]) {
									if (canPutRock(nnr, nnc, map, dogMap, itemMap, ninjas)) {
										stopList.add(new Point(nnr, nnc));
									}else {
										//置けないなら移動可能箇所として登録
										Point np = new Point(nnr, nnc);
										nlist.add(np);
										isMap[nnr][nnc] = true;
										ret++;
										//石置く場所が移動先なら消す
										stopList.remove(np);

									}
								}

							}else {//違うなら

							}
						}
					}
				}
			}
			//System.err.println();
			//後処理
			list = nlist;
		}
		return new PointsAndNum(stopList, ret);
	}

	//コピーした2次配列を返す
	public static int[][] copyMap(int[][] map) {
		int[][] copy = new int[map.length][];
		for (int i=0; i < map.length; i++) {
			copy[i] = Arrays.copyOf(map[i], map[i].length);
		}
		return copy;
	}
	//char型のコピー
	public static char[][] copyMap(char[][] map) {
		char[][] copy = new char[map.length][];
		for (int i=0; i < map.length; i++) {
			copy[i] = Arrays.copyOf(map[i], map[i].length);
		}
		return copy;
	}
	//double型のコピー
	public static double[][] copyMap(double[][] map) {
		double[][] copy = new double[map.length][];
		for (int i=0; i < map.length; i++) {
			copy[i] = Arrays.copyOf(map[i], map[i].length);
		}
		return copy;
	}
	//３次double型のコピー
	public static double[][][] copyMap(double[][][] map) {
		double[][][] copy = new double[map.length][map[0].length][];
		for (int i=0; i < map.length; i++) {
			for(int t=0; t < map[i].length; t++) {
				copy[i][t] = Arrays.copyOf(map[i][t], map[i][t].length);
			}
		}
		return copy;
	}
	//boolean型のコピー
	public static boolean[][] copyMap(boolean[][] map) {
		boolean[][] copy = new boolean[map.length][];
		for (int i=0; i < map.length; i++) {
			copy[i] = Arrays.copyOf(map[i], map[i].length);
		}
		return copy;
	}
	//３次float型のコピー
	public static float[][][] copyMap(float[][][] map) {
		float[][][] copy = new float[map.length][map[0].length][];
		for (int i=0; i < map.length; i++) {
			for(int t=0; t < map[i].length; t++) {
				copy[i][t] = Arrays.copyOf(map[i][t], map[i][t].length);
			}
		}
		return copy;
	}
	//float型のコピー
	public static float[][] copyMap(float[][] map) {
		float[][] copy = new float[map.length][];
		for (int i=0; i < map.length; i++) {
			copy[i] = Arrays.copyOf(map[i], map[i].length);
		}
		return copy;
	}
	//short型のコピー
	public static short[][] copyMap(short[][] map) {
		short[][] copy = new short[map.length][];
		for (int i=0; i < map.length; i++) {
			copy[i] = Arrays.copyOf(map[i], map[i].length);
		}
		return copy;
	}
	//Point型のコピー
	public static Point[][] copyMap(Point[][] map) {
		Point[][] copy = new Point[map.length][];
		for (int i=0; i < map.length; i++) {
			copy[i] = Arrays.copyOf(map[i], map[i].length);
		}
		return copy;
	}
}
