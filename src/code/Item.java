package code;

public class Item {

	public int row, col;

	public Item(int row, int col) {
		this.row = row;
		this.col = col;
	}
	public boolean equalPos(int r, int c) {
		return r == row && c == col;
	}
}
