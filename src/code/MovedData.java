package code;

import java.awt.Point;

//移動系算用に渡すデータ
public class MovedData implements Comparable<MovedData>{
	static final int PRI_RATE = 10000;
	//合計優先度
	public double pri;

	public int[] dirs;

	public Ninja[] ninjas;

	public char[][] map;

	public short[][] dogMap;

	public float[][] priorityMap;

	public boolean[][] itemMap;
	//null以外なら移動した石で移動前の座標を表す
	public Point[][] rockMoveMap;
	//自落雷出す場所。
	public int tr, tc;
	//自分身出す場所。
	public int ar, ac;
	//超高速使ったか
	public boolean isUseSpeed;
	//回転切り使ったか
	public boolean isUseSword;
	public MovedData(int[] dir, Ninja[] ninjas, char[][] map, short[][] dogMap
			, double pri,float[][] priorityMap, boolean[][] itemMap,int tx, int ty
			,Point[][] rockMoveMap, boolean isUseSpeed, int ar, int ac, boolean isUseSword) {
		super();
		this.dirs = dir;
		this.ninjas = ninjas;
		this.map = map;
		this.dogMap = dogMap;
		this.pri = pri;
		this.priorityMap = priorityMap;
		this.itemMap = itemMap;
		this.tr = tx;
		this.tc = ty;
		this.rockMoveMap = rockMoveMap;
		this.isUseSpeed = isUseSpeed;
		this.ar = ar;
		this.ac = ac;
		this.isUseSword = isUseSword;
	}
	@Override
	public int compareTo(MovedData o) {
		if (this.pri < o.pri) return -1;
		if (this.pri  > o.pri) return 1;
		return 0;
	}

}
