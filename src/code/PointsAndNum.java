package code;

import java.awt.Point;
import java.util.Set;

public class PointsAndNum {
	public Set<Point> points;
	public int num;

	public PointsAndNum(Set<Point> points, int num) {
		super();
		this.points = points;
		this.num = num;
	}

}
