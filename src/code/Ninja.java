package code;

//忍者
public class Ninja implements Cloneable{
	public int id;
	public int row;
	public int col;

	public Ninja(int id,int row, int col) {
		super();
		this.id = id;
		this.row = row;
		this.col = col;
	}

	public boolean equalPos(int r, int c) {
		return r == row && c == col;
	}
	@Override
	public Object clone(){
		
		return new Ninja(id, row, col);
	}
}
